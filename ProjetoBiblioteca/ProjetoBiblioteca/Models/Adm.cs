﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProjetoBiblioteca.Models
{
    public class Adm
    {
        private string email, nome, senha = "senai1234", areaAtuacao, cep, cidade, endereco, pais, uf, bairro;
        private int codUsuario;
        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
        public string Email
        {
            get { return email; }
            set
            { email = value; }
        }

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }

        public string Senha
        {
            get { return senha; }
            set { senha = "senai1234"; }

        }
        public string AreaAtuacao
        {
            get { return areaAtuacao; }
            set
            { areaAtuacao = value; }
        }

        public string Cep
        {
            get { return cep; }
            set { cep = value; }
        }

        public string Cidade
        {
            get { return cidade; }
            set { cidade = value; }

        }
        public string Endereco
        {
            get { return endereco; }
            set { endereco = value; }

        }

        public string Pais
        {
            get { return pais; }
            set { pais = value; }
        }

        public string Uf
        {
            get { return uf; }
            set { uf = value; }

        }
        public string Bairro
        {
            get { return bairro; }
            set { bairro = value; }

        }

        public int CodUsuario
        {
            get { return codUsuario; }
            set { codUsuario = value; }
        }

        public static Adm BuscaBiblio(string email)
        {
            Adm c = new Adm();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Usuario WHERE Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    c.Email = leitor["Email"].ToString();
                    c.Nome = leitor["Nome"].ToString();
                    c.Senha = leitor["Senha"].ToString();
                    c.CodUsuario = int.Parse(leitor["CodUsuario"].ToString());
                    c.AreaAtuacao = leitor["AreaAtuacao"].ToString();
                    c.Cep = leitor["CEP"].ToString();
                    c.Cidade = leitor["Cidade"].ToString();
                    c.Endereco = leitor["Endereço"].ToString();
                    c.Pais = leitor["País"].ToString();
                    c.Uf = leitor["UF"].ToString();
                    c.Bairro = leitor["Bairro"].ToString();
                }
            }
            catch (Exception e)
            {
                c = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return c;
        }

        internal string Editar()
        {
            string res = "Editado com sucesso!";
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("UPDATE Usuario SET " +
                    "Email = @Email, Nome = @Nome, CodUsuario = @CodUsuario, " +
                    "AreaAtuacao = @AreaAtuacao, CEP = @CEP, Cidade = @Cidade, Endereço = @Endereço, País = @País, UF = @UF, Bairro = @Bairro Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Nome", nome);
                query.Parameters.AddWithValue("@CodUsuario", codUsuario);
                query.Parameters.AddWithValue("@AreaAtuacao", areaAtuacao);
                query.Parameters.AddWithValue("@CEP", cep);
                query.Parameters.AddWithValue("@Cidade", cidade);
                query.Parameters.AddWithValue("@Endereço", endereco);
                query.Parameters.AddWithValue("@País", pais);
                query.Parameters.AddWithValue("@UF", uf);
                query.Parameters.AddWithValue("@Bairro", bairro);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }

        public string Cadastrar()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Usuario VALUES(@Email, @Nome,  @Senha, @CodUsuario, @AreaAtuacao, @CEP, @Cidade, @Endereço, @País, @UF, @Bairro)", con);
                query.Parameters.AddWithValue("@Email", email);
                query.Parameters.AddWithValue("@Nome", nome);
                query.Parameters.AddWithValue("@Senha", senha);
                query.Parameters.AddWithValue("@CodUsuario", codUsuario);
                query.Parameters.AddWithValue("@AreaAtuacao", areaAtuacao);
                query.Parameters.AddWithValue("@CEP", cep);
                query.Parameters.AddWithValue("@Cidade", cidade);
                query.Parameters.AddWithValue("@Endereço", endereco);
                query.Parameters.AddWithValue("@País", pais);
                query.Parameters.AddWithValue("@UF", uf);
                query.Parameters.AddWithValue("@Bairro", bairro);
                
                    query.ExecuteNonQuery();




            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Bibliotecário cadastrado com sucesso!";
        }

        public static List<Adm> ListaBiblio(string email)
        {
            List<Adm> lista = new List<Adm>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Usuario Where Email = @Email", con);
                query.Parameters.AddWithValue("@Email", email);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    Adm c = new Adm();
                    c.Email = leitor["Email"].ToString();
                    c.Nome = leitor["Nome"].ToString();
                    c.Senha = leitor["Senha"].ToString();
                    c.CodUsuario = int.Parse(leitor["CodUsuario"].ToString());
                    c.AreaAtuacao = leitor["AreaAtuacao"].ToString();
                    c.Cep = leitor["CEP"].ToString();
                    c.Cidade = leitor["Cidade"].ToString();
                    c.Endereco = leitor["Endereço"].ToString();
                    c.Pais = leitor["País"].ToString();
                    c.Uf = leitor["UF"].ToString();
                    c.Bairro = leitor["Bairro"].ToString();

                    lista.Add(c);
                }

            }
            catch (Exception e)
            {
                lista = new List<Adm>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        public static List<Adm> ListaTodos(string email)
        {
            List<Adm> lista = new List<Adm>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Usuario WHERE AreaAtuacao = 'Bibliotecário'", con);

                if (email != null)
                {
                    query.CommandText = query.CommandText + "Where Email = @Email";
                    query.Parameters.AddWithValue("@Email", email);

                }
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    Adm c = new Adm();
                    c.Email = leitor["Email"].ToString();
                    c.Nome = leitor["Nome"].ToString();
                    c.Senha = leitor["Senha"].ToString();
                    c.CodUsuario = int.Parse(leitor["CodUsuario"].ToString());
                    c.AreaAtuacao = leitor["AreaAtuacao"].ToString();
                    c.Cep = leitor["CEP"].ToString();
                    c.Cidade = leitor["Cidade"].ToString();
                    c.Endereco = leitor["Endereço"].ToString();
                    c.Pais = leitor["País"].ToString();
                    c.Uf = leitor["UF"].ToString();
                    c.Bairro = leitor["Bairro"].ToString();

                    lista.Add(c);
                }

            }
            catch (Exception e)
            {
                lista = new List<Adm>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        internal string Remover()
        {
            string res = "Bibliotecário removido com sucesso!";
            try
            {
                con.Open();


                SqlCommand deletar =
                new SqlCommand("DELETE FROM Usuario WHERE Email = @Email", con);
                deletar.Parameters.AddWithValue("@Email", email);
                deletar.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }



    }
}