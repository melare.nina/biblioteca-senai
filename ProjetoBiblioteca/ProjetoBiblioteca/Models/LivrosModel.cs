﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProjetoBiblioteca.Models
{
    public class LivrosModel
    {
        private string isbn, titulo, subtitulo, descritores, cidadePubli, editora, situacao = "Disponível", nomeAutor, imagemString, isbnzin, nomeAluno;
        private int serie, numTombo, edicao, anoPubli, numPag, codAutor, qtdAva = 0;
        private float avaliacao = 0;
        byte[] img;

        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
        public byte[] Img
        {
            get { return img; }
            set
            { img = value; }
        }

        public string ImagemString
        {
            get { return imagemString; }
            set
            { imagemString = value; }
        }

        public string Isbn
        {
            get { return isbn; }
            set
            { isbn = value; }
        }

        public int Serie
        {
            get { return serie; }
            set { serie = value; }
        }

        public string Subtitulo
        {
            get { return subtitulo; }
            set { subtitulo = value; }

        }

        public string Titulo
        {
            get { return titulo; }
            set { titulo = value; }

        }
        public string Descritores
        {
            get { return descritores; }
            set
            { descritores = value; }
        }

        public string NomeAutor
        {
            get { return nomeAutor; }
            set
            { nomeAutor = value; }
        }

        public string NomeAluno
        {
            get { return nomeAluno; }
            set
            { nomeAluno = value; }
        }


        public string CidadePubli
        {
            get { return cidadePubli; }
            set { cidadePubli = value; }
        }

        public string Editora
        {
            get { return editora; }
            set { editora = value; }

        }
        public string Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }


        public int NumTombo
        {
            get { return numTombo; }
            set { numTombo = value; }

        }
        public int Edicao
        {
            get { return edicao; }
            set { edicao = value; }

        }
        public int NumPag
        {
            get { return numPag; }
            set { numPag = value; }

        }

        public int AnoPubli
        {
            get { return anoPubli; }
            set { anoPubli = value; }

        }
        public int CodAutor
        {
            get { return codAutor; }
            set { codAutor = value; }

        }

        public int QtdAva
        {
            get { return qtdAva; }
            set { qtdAva = value; }

        }
        public float Avaliacao
        {
            get { return avaliacao; }
            set { avaliacao = value; }

        }

        public static List<LivrosModel> LerMais(int numTombo)
        {
            List<LivrosModel> lista = new List<LivrosModel>();
            try
            {
                con.Open();

                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Copiass c, Livros_Autoor la, Autoor a  WHERE l.isbn = c.isbn and l.isbn = la.ISBN and a.CodAutor = la.CodAutor AND c.NumeroDeTombo = @NumeroDeTombo", con);
                query.Parameters.AddWithValue("@NumeroDeTombo", numTombo);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    LivrosModel l = new LivrosModel();
                    l.Titulo = leitor["Titulo"].ToString();
                    l.Isbn = leitor["ISBN"].ToString();
                    l.Serie = int.Parse(leitor["Serie"].ToString());
                    l.Subtitulo = leitor["Subtitulo"].ToString();
                    l.Descritores = leitor["Descritores"].ToString();
                    l.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    l.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    l.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    l.Edicao = int.Parse(leitor["Edicao"].ToString());
                    l.CidadePubli = leitor["CidadePubli"].ToString();
                    l.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    l.Editora = leitor["Editora"].ToString();
                    l.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    l.Img = (byte[])leitor["imagem"];
                    l.Situacao = leitor["situacao"].ToString();
                    l.NomeAutor = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    l.ImagemString = Convert.ToBase64String(l.Img);

                    lista.Add(l);
                }
            }
            catch (Exception e)
            {
                lista = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        public static LivrosModel BuscaLivro(int? numTombo)
        {
            LivrosModel l = new LivrosModel();
            try
            {
                con.Open();

                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Copiass c, Livros_Autoor la, Autoor a  WHERE l.isbn = c.isbn and l.isbn = la.ISBN and a.CodAutor = la.CodAutor AND c.NumeroDeTombo = @NumeroDeTombo", con);



                query.Parameters.AddWithValue("@NumeroDeTombo", numTombo);


                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    l.Titulo = leitor["Titulo"].ToString();
                    l.Isbn = leitor["ISBN"].ToString();
                    l.Serie = int.Parse(leitor["Serie"].ToString());
                    l.Subtitulo = leitor["Subtitulo"].ToString();
                    l.Descritores = leitor["Descritores"].ToString();
                    l.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    l.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    l.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    l.Edicao = int.Parse(leitor["Edicao"].ToString());
                    l.CidadePubli = leitor["CidadePubli"].ToString();
                    l.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    l.Editora = leitor["Editora"].ToString();
                    l.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    l.Img = (byte[])leitor["imagem"];
                    l.Situacao = leitor["situacao"].ToString();
                    l.NomeAutor = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    l.ImagemString = Convert.ToBase64String(l.Img);

                }
            }
            catch (Exception e)
            {
                l = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return l;
        }

        internal string EditarCopia(int numeroTombo)
        {
            string res = "Cópia editada com sucesso!";
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("UPDATE Copiass SET " +
                    "NumeroDeTombo = @NumeroDeTomboo, Edicao = @Edicao, CidadePubli = @CidadePubli, AnoPubli = @AnoPubli, Editora = @Editora, NumPaginas = @NumPaginas, imagem = @imagem, ISBN = @ISBN Where NumeroDeTombo = @NumeroDeTombo", con);
                query.Parameters.AddWithValue("@NumeroDeTombo", numTombo);
                query.Parameters.AddWithValue("@NumeroDeTomboo", numeroTombo);
                query.Parameters.AddWithValue("@Edicao", edicao);
                query.Parameters.AddWithValue("@CidadePubli", cidadePubli);
                query.Parameters.AddWithValue("@AnoPubli", anoPubli);
                query.Parameters.AddWithValue("@Editora", editora);
                query.Parameters.AddWithValue("@NumPaginas", numPag);
                query.Parameters.AddWithValue("@imagem", img);
                query.Parameters.AddWithValue("@ISBN", isbn);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }

        internal string EditarLivro(string isbn)
        {
            string res = "Livro editado com sucesso!";
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("UPDATE Livros SET " +
                    "ISBN = @ISBN, Serie = @Serie, Titulo = @Titulo, Subtitulo = @Subtitulo, Descritores = @Descritores Where ISBN = @isbnzin ", con);
                query.Parameters.AddWithValue("@Titulo", titulo);
                query.Parameters.AddWithValue("@Serie", serie);
                query.Parameters.AddWithValue("@Subtitulo", subtitulo);
                query.Parameters.AddWithValue("@Descritores", descritores);
                query.Parameters.AddWithValue("@ISBN", isbn);
                query.Parameters.AddWithValue("@isbnzin", Isbn);
                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }

        public string BuscaIsbn(int serie)
        {
            try
            {
                con.Open();

                SqlCommand query = new SqlCommand("SELECT * FROM Livros WHERE Serie = @Serie", con);
                query.Parameters.AddWithValue("@Serie", serie);

                //Pegando a tabela, mandando executar e jogando ela na minha variável do tipo DataReader
                SqlDataReader leitor = query.ExecuteReader();

                if (leitor.HasRows)
                {
                    //Enquanto tiver linhas para ler

                    while (leitor.Read())
                    {
                        isbnzin = leitor["ISBN"].ToString();
                    }

                }
                con.Close();

            }
            catch (Exception ee)
            {
                return ee.Message;
            }
            return isbnzin;
        }

        public string CadastrarLivro()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Livros VALUES(@ISBN, @Serie,  @Titulo, @Subtitulo, @Descritores, @QtdAvaliadores, @Avaliacao)", con);
                query.Parameters.AddWithValue("@ISBN", isbn);
                query.Parameters.AddWithValue("@Serie", serie);
                query.Parameters.AddWithValue("@Titulo", titulo);
                query.Parameters.AddWithValue("@Subtitulo", subtitulo);
                query.Parameters.AddWithValue("@Descritores", descritores);
                query.Parameters.AddWithValue("@QtdAvaliadores", qtdAva);
                query.Parameters.AddWithValue("@Avaliacao", avaliacao);

                query.ExecuteNonQuery();


                query = new SqlCommand("INSERT INTO Livros_Autoor VALUES(@CodAutor, @ISBN)", con);
                query.Parameters.AddWithValue("@CodAutor", codAutor);
                query.Parameters.AddWithValue("@ISBN", isbn);
                query.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Livro cadastrado com sucesso!";
        }

        public string CadastrarCopia()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Copiass VALUES( @NumeroDeTombo, @Edicao, @CidadePubli, @AnoPubli, @Editora, @NumPaginas, @situacao, @imagem, @ISBN)", con);
                query.Parameters.AddWithValue("@ISBN", isbn);
                query.Parameters.AddWithValue("@NumeroDeTombo", numTombo);
                query.Parameters.AddWithValue("@NumPaginas", numPag);
                query.Parameters.AddWithValue("@Edicao", edicao);
                query.Parameters.AddWithValue("@CidadePubli", cidadePubli);
                query.Parameters.AddWithValue("@AnoPubli", anoPubli);
                query.Parameters.AddWithValue("@Editora", editora);
                query.Parameters.AddWithValue("@Avaliacao", avaliacao);
                query.Parameters.AddWithValue("@situacao", situacao);
                query.Parameters.AddWithValue("@imagem", img);
                query.ExecuteNonQuery();


            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Cópia cadastrada com sucesso!";
        }



        public static List<LivrosModel> Lista(string titulo)
        {
            List<LivrosModel> lista = new List<LivrosModel>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Copiass c, Livros_Autoor la, Autoor a WHERE l.isbn= c.isbn and l.isbn = la.ISBN and a.CodAutor = la.CodAutor ", con);





                if (titulo != "")
                {
                    query.CommandText = query.CommandText + "AND l.Titulo = @Titulo";
                    query.Parameters.AddWithValue("@Titulo", titulo);

                }
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    LivrosModel l = new LivrosModel();
                    l.Titulo = leitor["Titulo"].ToString();
                    l.Isbn = leitor["ISBN"].ToString();
                    l.Serie = int.Parse(leitor["Serie"].ToString());
                    l.Subtitulo = leitor["Subtitulo"].ToString();
                    l.Descritores = leitor["Descritores"].ToString();
                    l.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    l.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    l.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    l.Edicao = int.Parse(leitor["Edicao"].ToString());
                    l.CidadePubli = leitor["CidadePubli"].ToString();
                    l.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    l.Editora = leitor["Editora"].ToString();
                    l.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    l.Img = (byte[])leitor["imagem"];
                    l.Situacao = leitor["situacao"].ToString();
                    l.NomeAutor = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    l.ImagemString = Convert.ToBase64String(l.Img);

                    lista.Add(l);
                }

            }
            catch (Exception e)
            {
                lista = new List<LivrosModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }


        public static List<LivrosModel> ListaEmprestados()
        {
            List<LivrosModel> lista = new List<LivrosModel>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Usuario u, Emprestimos e, Copiass c, Livros_Autoor la, Autoor a WHERE l.isbn= c.isbn and e.NumeroDeTombo = c.NumeroDeTombo and e.EmailUsuario = u.Email and l.isbn = la.ISBN and a.CodAutor = la.CodAutor AND situacao = 'Emprestado' AND DataDevolu >  GETDATE()", con);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    LivrosModel l = new LivrosModel();
                    l.Titulo = leitor["Titulo"].ToString();
                    l.Isbn = leitor["ISBN"].ToString();
                    l.Serie = int.Parse(leitor["Serie"].ToString());
                    l.Subtitulo = leitor["Subtitulo"].ToString();
                    l.Descritores = leitor["Descritores"].ToString();
                    l.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    l.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    l.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    l.Edicao = int.Parse(leitor["Edicao"].ToString());
                    l.CidadePubli = leitor["CidadePubli"].ToString();
                    l.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    l.Editora = leitor["Editora"].ToString();
                    l.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    l.Img = (byte[])leitor["imagem"];
                    l.Situacao = leitor["situacao"].ToString();
                    l.NomeAluno = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    l.ImagemString = Convert.ToBase64String(l.Img);


                    lista.Add(l);


                }

            }
            catch (Exception e)
            {
                lista = new List<LivrosModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        public static List<LivrosModel> ListraAtrasados()
        {
            List<LivrosModel> lista = new List<LivrosModel>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Usuario u, Copiass c, Livros_Autoor la, Emprestimos e, Autoor a WHERE e.NumeroDeTombo = c.NumeroDeTombo AND l.isbn= c.isbn and e.NumeroDeTombo = c.NumeroDeTombo and e.EmailUsuario = u.Email and l.isbn = la.ISBN and a.CodAutor = la.CodAutor AND DataDevolu <  GETDATE()", con);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    LivrosModel lA = new LivrosModel();
                    lA.Titulo = leitor["Titulo"].ToString();
                    lA.Isbn = leitor["ISBN"].ToString();
                    lA.Serie = int.Parse(leitor["Serie"].ToString());
                    lA.Subtitulo = leitor["Subtitulo"].ToString();
                    lA.Descritores = leitor["Descritores"].ToString();
                    lA.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    lA.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    lA.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    lA.Edicao = int.Parse(leitor["Edicao"].ToString());
                    lA.CidadePubli = leitor["CidadePubli"].ToString();
                    lA.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    lA.Editora = leitor["Editora"].ToString();
                    lA.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    lA.Img = (byte[])leitor["imagem"];
                    lA.Situacao = leitor["situacao"].ToString();
                    lA.NomeAluno = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    lA.ImagemString = Convert.ToBase64String(lA.Img);


                    lista.Add(lA);
                    
                }

            }
            catch (Exception e)
            {
                lista = new List<LivrosModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }


        public static List<LivrosModel> ListaTodos()
        {
            List<LivrosModel> lista = new List<LivrosModel>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Copiass c, Livros_Autoor la, Autoor a WHERE l.isbn= c.isbn and l.isbn = la.ISBN and a.CodAutor = la.CodAutor", con);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    LivrosModel l = new LivrosModel();
                    l.Titulo = leitor["Titulo"].ToString();
                    l.Isbn = leitor["ISBN"].ToString();
                    l.Serie = int.Parse(leitor["Serie"].ToString());
                    l.Subtitulo = leitor["Subtitulo"].ToString();
                    l.Descritores = leitor["Descritores"].ToString();
                    l.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    l.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    l.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    l.Edicao = int.Parse(leitor["Edicao"].ToString());
                    l.CidadePubli = leitor["CidadePubli"].ToString();
                    l.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    l.Editora = leitor["Editora"].ToString();
                    l.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    l.Img = (byte[])leitor["imagem"];
                    l.Situacao = leitor["situacao"].ToString();
                    l.NomeAutor = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    l.ImagemString = Convert.ToBase64String(l.Img);

                    if (l.Situacao.Equals("Disponível"))
                    {
                        lista.Add(l);
                    }


                }

            }
            catch (Exception e)
            {
                lista = new List<LivrosModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }



        internal string Remover()
        {
            string res = "Livro removido com sucesso!";
            try
            {
                con.Open();


                SqlCommand deletar =
                new SqlCommand("DELETE FROM Livros WHERE ISBN = @ISBN", con);
                deletar.Parameters.AddWithValue("@ISBN", isbn);
                deletar.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }

        internal string RemoverCopia()
        {
            string res = "Cópia removida com sucesso!";
            try
            {
                con.Open();


                SqlCommand deletar =
                new SqlCommand("DELETE FROM Copiass WHERE NumeroDeTombo = @NumeroDeTombo", con);
                deletar.Parameters.AddWithValue("@NumeroDeTombo", numTombo);
                deletar.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }
    }
}