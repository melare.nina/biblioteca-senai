﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProjetoBiblioteca.Models
{
    public class EmprestimoModelo
    {
        private string situacao, emailUsuario;
        private DateTime dataRtirada, dataRenova, dataDevolucao, dataDevolucaoD = new DateTime();
        private int renovacao = 0, multa = 0, numerodeTombo;
        public string s;


        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);

        public string Situacao
        {
            get { return situacao; }
            set { situacao = value; }
        }
        public string EmailUsuario
        {
            get { return emailUsuario; }
            set { emailUsuario = value; }
        }
        public DateTime DataRtirada
        {
            get { return dataRtirada; }
            set { dataRtirada = value; }
        }
        public DateTime DataDevolucao
        {
            get { return dataDevolucao; }
            set { dataDevolucao = value; }
        }
        public int Renovacao
        {
            get { return renovacao; }
            set { renovacao = value; }
        }
        public int Multa
        {
            get { return multa; }
            set { multa = value; }
        }
        public int NumerodeTombo
        {
            get { return numerodeTombo; }
            set { numerodeTombo = value; }
        }


        public string Emprestimo()
        {
            try
            {
                con.Open();

                /*-----------------------------------------------------Testando se tem multa------------------------------------------------------*/
                SqlCommand query = new SqlCommand("SELECT * FROM Devolucao Where EmailUsuario  = @EmailUsuario", con);
                query.Parameters.AddWithValue("@EmailUsuario", emailUsuario);
                SqlDataReader leitor = query.ExecuteReader();





                while (leitor.Read())
                {
                    multa = int.Parse(leitor["Multa"].ToString());
                    dataDevolucaoD = (DateTime)leitor["DataDevoluD"];
                }

                leitor.Close();


                if (multa > 0)
                {
                    dataDevolucaoD = dataDevolucaoD.AddDays(multa);

                    if (DateTime.Today.Subtract(dataDevolucaoD).Days < 0)
                    {
                        return "Usuário impossibilitado de emprestar devido a Multa. Só poderá emprestar a partir do dia: " + dataDevolucaoD;
                    }

                }


                /*-----------------------------------------------------Testando se tem multa------------------------------------------------------*/

                query = new SqlCommand("SELECT * FROM Copiass Where NumeroDeTombo  = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    s = leitor["situacao"].ToString();
                }


                leitor.Close();

                if (s == "Disponível")
                {
                    if (dataRtirada == DateTime.Today)
                    {

                        query = new SqlCommand("INSERT INTO Emprestimos VALUES(@dataR, @datad, @renovacao, @multa, @ndetombo, @emailusu)", con);
                        query.Parameters.AddWithValue("@dataR", dataRtirada);
                        query.Parameters.AddWithValue("@dataD", dataDevolucao);
                        query.Parameters.AddWithValue("@renovacao", renovacao);
                        query.Parameters.AddWithValue("@multa", multa);
                        query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                        query.Parameters.AddWithValue("@emailusu", emailUsuario);
                        query.ExecuteNonQuery();


                        query = new SqlCommand("UPDATE Copiass SET " +
                                            "situacao = 'Emprestado' Where NumeroDeTombo = @NumeroDeTombo", con);
                        query.Parameters.AddWithValue("@NumeroDeTombo", numerodeTombo);

                        query.ExecuteNonQuery();

                        con.Close();


                        return "Empréstimo efetuado com sucesso";
                    }
                    else
                    {
                        return "Só é possível emprestar na data de hoje!";
                    }
                }
                else
                {
                    return "Livro indisponivel";
                }

               



            }

            catch (Exception ee)
            {
                return (ee.Message);
            }


        }


        public string Reservar()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos


                SqlCommand query = new SqlCommand("SELECT * FROM Copiass Where NumeroDeTombo  = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    EmprestimoModelo l = new EmprestimoModelo();
                    s = leitor["situacao"].ToString();
                }



                leitor.Close();

                if (s == "Disponível")
                {



                    query = new SqlCommand("INSERT INTO Reservas VALUES(@dataReserva, @ndetombo, @emailusu)", con);
                    query.Parameters.AddWithValue("@dataReserva", DateTime.Today.AddDays(1));
                    query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                    query.Parameters.AddWithValue("@emailusu", emailUsuario);
                    query.ExecuteNonQuery();


                    query = new SqlCommand("UPDATE Copiass SET " +
                                        "situacao = 'Emprestado e Reservado' Where NumeroDeTombo = @NumeroDeTombo", con);
                    query.Parameters.AddWithValue("@NumeroDeTombo", numerodeTombo);

                    query.ExecuteNonQuery();

                    con.Close();


                    return "Reserva efetuada com sucesso";
                }
                else
                {



                    if (s == "Emprestado")
                    {

                        query = new SqlCommand("SELECT * FROM Emprestimos Where NumeroDeTombo = @ntombo", con);
                        query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                        leitor = query.ExecuteReader();

                        while (leitor.Read())
                        {
                            dataDevolucao = (DateTime)leitor["DataDevolu"];

                        }

                        leitor.Close();

                        dataDevolucao = dataDevolucao.AddDays(1);
                        query = new SqlCommand("INSERT INTO Reservas VALUES(@dataReserva, @ndetombo, @emailusu)", con);
                        query.Parameters.AddWithValue("@dataReserva", dataDevolucao);
                        query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                        query.Parameters.AddWithValue("@emailusu", emailUsuario);
                        query.ExecuteNonQuery();


                        query = new SqlCommand("UPDATE Copiass SET " +
                                            "situacao = 'Emprestado e Reservado' Where NumeroDeTombo = @NumeroDeTombo", con);
                        query.Parameters.AddWithValue("@NumeroDeTombo", numerodeTombo);

                        query.ExecuteNonQuery();


                        con.Close();

                        return "Reserva efetuada com sucesso";
                    }
                    else
                    {
                        con.Close();
                        return "Livro indisponivel";
                    }

                }

                



            }

            catch (Exception ee)
            {
                return (ee.StackTrace);
            }


        }

        public string Renovar()
        {

            try
            {
                con.Open();

                SqlCommand query = new SqlCommand("SELECT * FROM Emprestimos Where NumeroDeTombo = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    dataRenova = (DateTime)leitor["DataDevolu"];
                    renovacao = int.Parse(leitor["Renovacao"].ToString());
                }



                leitor.Close();

                if (renovacao <= 3)
                {
                    dataRenova = dataRenova.AddDays(15);


                    query = new SqlCommand("UPDATE Emprestimos SET DataDevolu = @dataD, Renovacao = @renovacao WHERE NumeroDeTombo = @ndetombo", con);
                    query.Parameters.AddWithValue("@dataD", dataRenova);
                    query.Parameters.AddWithValue("@renovacao", renovacao + 1);
                    query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                    query.ExecuteNonQuery();



                }
                else
                {
                    return "Não foi possível realizar a renovação";
                }

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }



            }

            catch (Exception ee)
            {
                return (ee.Message);
            }

            return "Renovação efetuada com sucesso";

        }

        public string Devolver()
        {

            try
            {
                con.Open();

                /*-------------------------------Buscando DATAS-------------------------------------------*/
                SqlCommand query = new SqlCommand("SELECT * FROM Emprestimos Where NumeroDeTombo = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    dataRtirada = (DateTime)leitor["DataRetirada"];
                    dataDevolucao = (DateTime)leitor["DataDevolu"];
                }

                leitor.Close();
                /*-------------------------------Buscando DATAS-------------------------------------------*/




                /*-------------------------------Buscando Situação do Livro-------------------------------------------*/
                query = new SqlCommand("SELECT * FROM Copiass Where NumeroDeTombo  = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    EmprestimoModelo l = new EmprestimoModelo();
                    s = leitor["situacao"].ToString();
                }
                leitor.Close();
                /*-------------------------------Buscando Situação do Livro-------------------------------------------*/

                //Inserindo na tabela devolução
                query = new SqlCommand("INSERT INTO Devolucao VALUES(@DataRetiradaD, @DataDevoluD, @multa, @NumeroDeTombo, @EmailUsuario)", con);
                query.Parameters.AddWithValue("@DataRetiradaD", dataRtirada);
                query.Parameters.AddWithValue("@DataDevoluD", DateTime.Today);
                query.Parameters.AddWithValue("@multa", multa);
                query.Parameters.AddWithValue("@NumeroDeTombo", numerodeTombo);
                query.Parameters.AddWithValue("@EmailUsuario", emailUsuario);
                query.ExecuteNonQuery();

                int atraso;
                //Comparando as datas
                atraso = DateTime.Compare(dataDevolucao, DateTime.Today);

                //Se a data de devolução for maior que a de hoje
                if (atraso < 0)
                {
                    //Subtraindo a quantidade de dias de diferença
                    TimeSpan dias = DateTime.Today.Subtract(dataDevolucao);

                    //Calculando a multa
                    multa = 3 * dias.Days;

                    query = new SqlCommand("UPDATE Devolucao SET Multa = @multa WHERE NumeroDeTombo = @ndetombo", con);
                    query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                    query.Parameters.AddWithValue("@multa", multa);
                    query.ExecuteNonQuery();
                }


                if (s == "Emprestado")
                {
                    //Atualizando a situação do livro
                    query = new SqlCommand("UPDATE Copiass SET situacao = @situacao WHERE NumeroDeTombo = @ndetombo", con);
                    query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                    query.Parameters.AddWithValue("@situacao", "Disponível");
                    query.ExecuteNonQuery();
                }

                //Deletando da tabela de empréstimos
                query = new SqlCommand("DELETE FROM Emprestimos WHERE NumeroDeTombo = @ndetombo", con);
                query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                query.ExecuteNonQuery();



                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }



            }

            catch (Exception ee)
            {
                return (ee.Message);
            }

            return "Devolução efetuada com sucesso";

        }



        public string EmailUsu()
        {
            string email;
            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos


                SqlCommand query = new SqlCommand("SELECT * FROM Emprestimos Where NumeroDeTombo  = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                SqlDataReader leitor = query.ExecuteReader();

                email = leitor["EmailUsuario"].ToString();

                query.ExecuteNonQuery();


                return email;

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }

            }

            catch (Exception ee)
            {
                return (ee.Message);
            }


        }



    }


}

