﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ProjetoBiblioteca.Models
{
    public class AutoresModel
    {
        private string nomeAutor, nomeAutorEdit;
        private int cod;

        static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);

        public string NomeAutor
        {
            get { return nomeAutor; }
            set { nomeAutor = value; }
        }

        public string NomeAutorEdit
        {
            get { return nomeAutorEdit; }
            set { nomeAutorEdit = value; }
        }

        public int Cod
        {
            get { return cod; }
            set { cod = value; }
        }


        public string CadastrarAutor()
        {

            try
            {
                con.Open();

                //Objeto que faz comandos           //comandos usando o @ para ficar mais seguro        //Conexão que faz os comandos
                SqlCommand query = new SqlCommand("INSERT INTO Autoor VALUES(@Nome)", con);
                query.Parameters.AddWithValue("@Nome", nomeAutor);

                query.ExecuteNonQuery();

            }
            catch (Exception ee)
            {
                return (ee.Message);
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }


            return "Autor cadastrado com sucesso!";
        }

        // :)
        public static List<AutoresModel> Lista(string nome)
        {
            List<AutoresModel> lista = new List<AutoresModel>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Autoor ", con);
                
                if (nome != "")
                {
                    query.CommandText = query.CommandText + "WHERE Nome = @Nome";
                    query.Parameters.AddWithValue("@Nome", nome);

                }
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    AutoresModel a = new AutoresModel();
                    a.NomeAutor = leitor["Nome"].ToString();
                    a.Cod = int.Parse(leitor["CodAutor"].ToString());

                    lista.Add(a);
                }

            }
            catch (Exception e)
            {
                lista = new List<AutoresModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        public static List<AutoresModel> ListaTodos()
        {
            List<AutoresModel> lista = new List<AutoresModel>();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Autoor", con);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    AutoresModel a = new AutoresModel();
                    a.NomeAutor = leitor["Nome"].ToString();
                    a.Cod = int.Parse(leitor["CodAutor"].ToString());

                    lista.Add(a);
                }

            }
            catch (Exception e)
            {
                lista = new List<AutoresModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }

        public static AutoresModel BuscaAutor(string nome)
        {
            AutoresModel a = new AutoresModel();
            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Autoor WHERE Nome = @Nome", con);
                query.Parameters.AddWithValue("@Nome", nome);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    a.NomeAutor = leitor["Nome"].ToString();
                    a.Cod = int.Parse(leitor["CodAutor"].ToString());

                }
            }
            catch (Exception e)
            {
                a = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return a;
        }

        internal string Editar()
        {
            string res = "Editado com sucesso!";
            AutoresModel a = new AutoresModel();
            try
            {
                con.Open();

                SqlCommand query1 =
                   new SqlCommand("SELECT * FROM Autoor WHERE Nome = @Nome", con);
                query1.Parameters.AddWithValue("@Nome", nomeAutor);
                SqlDataReader leitor = query1.ExecuteReader();

                while (leitor.Read())
                {
                    a.Cod = int.Parse(leitor["CodAutor"].ToString());
                }

                con.Close();

                con.Open();

                query1 = new SqlCommand("UPDATE Autoor SET Nome = @Nome Where CodAutor = @CodAutor", con); 
                    
                query1.Parameters.AddWithValue("@CodAutor", a.Cod);
                query1.Parameters.AddWithValue("@Nome", nomeAutorEdit);
                query1.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == System.Data.ConnectionState.Open)
                con.Close();

            return res;
        }

        internal string Remover()
        {
            string res = "Removido com sucesso!";

            try
            {
                con.Open();
                SqlCommand query =
                    new SqlCommand("DELETE FROM Autoor WHERE CodAutor = @CodAutor", con);
                query.Parameters.AddWithValue("@CodAutor", Cod);

                query.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                res = e.Message;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return res;

        }

    }
}