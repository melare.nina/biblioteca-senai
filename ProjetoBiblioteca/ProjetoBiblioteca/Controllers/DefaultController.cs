﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjetoBiblioteca.Controllers
{
    public class DefaultController : Controller
    {
        // GET: Default
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Salvar()
        {
            //salvar no banco
            foreach (string nomeArq in Request.Files)
            {
                HttpPostedFileBase arquivoPostado = Request.Files[nomeArq];
                int tamanhoImg = arquivoPostado.ContentLength; //pega o tamanho do arquivo
                string tipoArq = arquivoPostado.ContentType; //pega o tipo do arquivo

                //se existe a palavra jpeg dentro do arquivo
                if (tipoArq.IndexOf("jpeg") > 0)
                {
                    byte[] imgBytes = new byte[tamanhoImg];
                    arquivoPostado.InputStream.Read(imgBytes, 0, tamanhoImg); //Ler do primeiro byte (0) até o último (tamanho da imagem) e guardar na variável imgBytes

                    //Puxando das configurações já estabelecidas
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
                    con.Open();
                    SqlCommand query = new SqlCommand("INSERT INTO imgg VALUES(@imagem)", con);
                    query.Parameters.AddWithValue("@imagem", imgBytes);
                    query.ExecuteNonQuery();

                }

              
            }

            return RedirectToAction("Index");
        }

        public ActionResult Listar()
        {
            List<string> listaImg = new List<string>();

            //Puxando das configurações já estabelecidas
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["bcd"].ConnectionString);
            con.Open();
            SqlCommand query = new SqlCommand("SELECT * FROM imgg", con);
            SqlDataReader leitor = query.ExecuteReader();

            while (leitor.Read())
            {
                byte[] imgBytes = (byte[]) leitor["imagem"];

                //Convertendo pra string
                listaImg.Add(Convert.ToBase64String(imgBytes));
            }

            con.Close();

            return View(listaImg);
        }
    }
}