﻿using ProjetoBiblioteca.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ProjetoBiblioteca.Controllers
{
    public class AdmController : Controller
    {
        // GET: Adm
        public ActionResult Index()
        {
            Session["Lista"] = null;
            return View();
        }

        public ActionResult InfoBiblio()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult InfoBiblio(string email)
        {
            Session["Lista"] = "tem";
            Adm a = new Adm();
            a.Email = email;
            return View(Adm.ListaTodos(email));
        }

        public ActionResult CadastroBiblio()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CadastroBiblio( string nome, string email, int nif, string areaAtuacao, string cep, string cidade, string endereco, string pais, string uf, string bairro)
        {
            Session["Lista"] = null;
            Adm a = new Adm();
            a.Email = email;
            a.Nome = nome;
            a.CodUsuario = nif;
            a.AreaAtuacao = areaAtuacao;
            a.Cep = cep;
            a.Cidade = cidade;
            a.Endereco = endereco;
            a.Pais = pais;
            a.Uf = uf;
            a.Bairro = bairro;

            TempData["Msg"] = a.Cadastrar();

            if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
            {
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress("bibliotecas509@gmail.com");
                //Destinatário
                mail.To.Add(email);
                //Assunto
                mail.Subject = "Cadastro no Sistema de Empréstimo de Livros";
                //Corpo do e-mail
                mail.Body = "Olá " + nome + "!\n\nVocê foi cadastrado no Sistema de Empréstimos de Livros do SENAI ZERBINI como BIBLIOTECÁRIO\n\nSeu usuário é o seu email: " + email + " e sua senha será: senai1234\n\nVocê poderá alterá-la pelo aplicativo mobile\n\nBoa leitura!";

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential("bibliotecas509@gmail.com", "biblio509");

                //Envia
                smtpServer.Send(mail);
            }

            return RedirectToAction("CadastroBiblio");
        }

        public ActionResult Editar(string email)
        {
            Session["Lista"] = null;
            Adm a = Adm.BuscaBiblio(email);

            if (a == null)
            {
                TempData["Msg"] = "Bibliotecário não encontrado no sistema!";
                return RedirectToAction("Alunos");
            }

            return View(a);
        }

        [HttpPost]
        public ActionResult Editar(string Email, string Nome, int CodUsuario, string AreaAtuacao, string Cep, string Cidade, string Endereco, string Pais, string Uf, string Bairro)
        {
            Session["Lista"] = null;
            Adm c = new Adm();
            c.Email = Email;
            c.Nome = Nome;
            c.CodUsuario = CodUsuario;
            c.AreaAtuacao = AreaAtuacao;
            c.Cep = Cep;
            c.Cidade = Cidade;
            c.Endereco = Endereco;
            c.Pais = Pais;
            c.Uf = Uf;
            c.Bairro = Bairro;

            string res = c.Editar();
            TempData["Msg"] = res;
            if (res == "Editado com sucesso!")
            {
                return RedirectToAction("InfoBiblio");
            }
            else
            {
                return RedirectToAction("Editar");
            }
        }

        public ActionResult Excluir(string email)
        {
            Adm c = new Adm();
            c.Email = email;
            TempData["Msg"] = c.Remover();
            return RedirectToAction("InfoBiblio");
        }

        // :)
    }
}