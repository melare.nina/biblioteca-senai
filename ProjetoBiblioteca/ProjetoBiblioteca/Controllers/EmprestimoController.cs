﻿using ProjetoBiblioteca.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace ProjetoBiblioteca.Controllers
{
    public class EmprestimoController : Controller
    {
        // GET: Emprestimo
        public ActionResult Emprestimos()
        {

            return View(CadastroAlunos.ListaTodos());
        }

        [WebMethod]
        public ActionResult BuscarAluno(string email)
        {
            return Json(CadastroAlunos.BuscaAluno(email), JsonRequestBehavior.AllowGet);

            //return RedirectToAction("Emprestimos", CadastroAlunos.BuscaAluno(email));
        }

        public ActionResult BuscarLivro(string titulo)
        {
            return Json(LivrosModel.Lista(titulo), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Emprestar(string email, int livro, DateTime dataEmprestimo, DateTime dataRetirada)
        {
            EmprestimoModelo e = new EmprestimoModelo();

            e.EmailUsuario = email;
            e.DataRtirada = dataEmprestimo;
            e.DataDevolucao = dataRetirada;
            e.NumerodeTombo = livro;
            TempData["Msg"] = e.Emprestimo();

            if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
            {
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress("bibliotecas509@gmail.com");
                //Destinatário
                mail.To.Add(email);
                //Assunto
                mail.Subject = "Empréstimo de Livro no Sistema de Empréstimo de Livros";
                //Corpo do e-mail
                mail.Body = "Olá!\n\nO Empréstimo do seu livro de número de tombo" + livro + " foi realizado com sucesso!\n\nA data de entrega será em: " + dataRetirada.ToString("MM/dd/yyyy") + "\n\nBoa Leitura!";

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential("bibliotecas509@gmail.com", "biblio509");

                //Envia
                smtpServer.Send(mail);
            }

            return RedirectToAction("Emprestimos");
        }


        [HttpPost]
        public ActionResult Devolver(string email, int livro)
        {
            EmprestimoModelo e = new EmprestimoModelo();

            e.EmailUsuario = email;
            e.NumerodeTombo = livro;
            TempData["Msg"] = e.Devolver();

           
            return RedirectToAction("Emprestimos");
        }

        [HttpPost]
        public ActionResult Reservar(string email, int livro)
        {
            EmprestimoModelo e = new EmprestimoModelo();

            e.EmailUsuario = email;
            e.NumerodeTombo = livro;
            TempData["Msg"] = e.Reservar();

            if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
            {
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress("bibliotecas509@gmail.com");
                //Destinatário
                mail.To.Add(email);
                //Assunto
                mail.Subject = "Livro Reservado no Sistema de Empréstimo de Livros";
                //Corpo do e-mail
                mail.Body = "Olá!\n\nA reserva do seu livro de número de tombo: " + livro + " foi realizada com sucesso!\n\nBoa Leitura!";

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential("bibliotecas509@gmail.com", "biblio509");

                //Envia
                smtpServer.Send(mail);
            }


            return RedirectToAction("Emprestimos");
        }



        [HttpPost]
        public ActionResult Renovar(string email, int livro)
        {
            EmprestimoModelo e = new EmprestimoModelo();

            e.EmailUsuario = email;
            e.NumerodeTombo = livro;
            TempData["Msg"] = e.Renovar();

            if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
            {
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress("bibliotecas509@gmail.com");
                //Destinatário
                mail.To.Add(email);
                //Assunto
                mail.Subject = "Renovação de Livro no Sistema de Empréstimo de Livros";
                //Corpo do e-mail
                mail.Body = "Olá!\n\nA renovação do seu livro de número de tombo: " + livro + " foi realizada com sucesso!\n\nA NOVA data de entrega será quinze dias após a data de devolução do livro.\n\nBoa Leitura!";

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential("bibliotecas509@gmail.com", "biblio509");

                //Envia
                smtpServer.Send(mail);
            }

            return RedirectToAction("Emprestimos");
        }

    }
}