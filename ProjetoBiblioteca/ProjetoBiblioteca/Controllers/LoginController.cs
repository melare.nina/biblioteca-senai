﻿using ProjetoBiblioteca.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjetoBiblioteca.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        
            // :)
        public ActionResult Login()
        {

            return View();
        }


        [HttpPost]
        public ActionResult Login(string usuario, string senha)
        {
            LoginWeb l = new LoginWeb();


            l.Senha = senha;
            l.Email = usuario;

            if (l.Compara(usuario, senha))
            {

                Session["auxE"] = usuario;

                if (l.TipoUsu(usuario, senha).ToString().Equals("Administrador"))
                {
                    Session["Type"] = "Administrador";
                    return RedirectToAction("Index", "Adm");
                }
                if (l.TipoUsu(usuario, senha).ToString().Equals("Bibliotecário"))
                {
                    Session["Type"] = "Bibliotecário";
                    return RedirectToAction("Index", "Biblio");
                }

            }



            return RedirectToAction("Login", "Login");
        }
        public ActionResult Sair()
        {
            Session["aux"] = null;
            return RedirectToAction("Login", "Login");
        }


    }
}