﻿using ProjetoBiblioteca.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace ProjetoBiblioteca.Controllers
{
    public class BiblioController : Controller
    {
        // GET: Biblio
        public ActionResult Index()
        {
            Session["Lista"] = null;

            ViewBag.Atrasados = LivrosModel.ListraAtrasados();

            return View(LivrosModel.ListaEmprestados());
        }
        /*----------------------------------------------------------ALUNO--------------------------------------------------------------*/
        public ActionResult Alunos()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Alunos(string email)
        {
            Session["Lista"] = "tem";
            return View(CadastroAlunos.ListaAlunos(email));
        }

        

        public ActionResult CadastroAluno()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastroAluno(string email, string nome, string telefone, int matricula, string areaAtuacao, string cep, string cidade, string endereco, string pais, string uf, string bairro)
        {
            Session["Lista"] = null;
            CadastroAlunos c = new CadastroAlunos();
            c.Email = email;
            c.Nome = nome;
            c.CodUsuario = matricula;
            c.AreaAtuacao = areaAtuacao;
            c.Cep = cep;
            c.Cidade = cidade;
            c.Endereco = endereco;
            c.Pais = pais;
            c.Uf = uf;
            c.Bairro = bairro;
            c.Telefone = telefone;

            TempData["Msg"] = c.Cadastrar();

            if (TempData["Msg"].ToString().IndexOf("sucesso") > 0)
            {
                //Configurando a mensagem
                MailMessage mail = new MailMessage();
                //Origem
                mail.From = new MailAddress("bibliotecas509@gmail.com");
                //Destinatário
                mail.To.Add(email);
                //Assunto
                mail.Subject = "Cadastro no Sistema de Empréstimo de Livros";
                //Corpo do e-mail
                mail.Body = "Olá " + nome + "!\n\nVocê foi cadastrado no Sistema de Empréstimos de Livros do SENAI ZERBINI\n\nSeu usuário é o seu email: "+email+" e sua senha será: senai1234\n\nVocê poderá alterá-la pelo aplicativo mobile\n\nBoa leitura!";

                //Configurar o smtp
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                //configurou porta
                smtpServer.Port = 587;
                //Habilitou o TLS
                smtpServer.EnableSsl = true;
                //Configurou usuario e senha p/ logar
                smtpServer.Credentials = new System.Net.NetworkCredential("bibliotecas509@gmail.com", "biblio509");

                //Envia
                smtpServer.Send(mail);
            }

            return RedirectToAction("CadastroAluno");
        }

        public ActionResult EditarAluno(string email)
        {
            Session["Lista"] = null;
            CadastroAlunos c = CadastroAlunos.BuscaAluno(email);

            if(c == null)
            {
                TempData["Msg"] = "Aluno não encontrado no sistema!";
                return RedirectToAction("Alunos");
            }

            return View(c);
        }

        [HttpPost]
        public ActionResult EditarAluno(string email, string nome, string CodUsuario, string telefone, string areaAtuacao, string cep, string cidade, string endereco, string pais, string uf, string bairro)
        {
            Session["Lista"] = null;
            CadastroAlunos c = new CadastroAlunos();
            c.Email = email;
            c.Nome = nome;
            c.CodUsuario = int.Parse(CodUsuario);
            c.AreaAtuacao = areaAtuacao;
            c.Cep = cep;
            c.Cidade = cidade;
            c.Endereco = endereco;
            c.Pais = pais;
            c.Uf = uf;
            c.Bairro = bairro;
            c.Telefone = telefone;

            string res = c.Editar();
            TempData["Msg"] = res;
            if (res == "Editado com sucesso!") {
                return RedirectToAction("Alunos");
            }
            else
            {
                 return RedirectToAction("EditarAluno");
            }
        }

        public ActionResult ExcluirAluno(string email)
        {
            CadastroAlunos c = new CadastroAlunos();
            c.Email = email;
            TempData["Msg"] = c.Remover();
            return RedirectToAction("Alunos");
        }
        /*----------------------------------------------------------ALUNO--------------------------------------------------------------*/



            //
        /*------------------------------------------------------Professor--------------------------------------------------------------*/
        public ActionResult Professores()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Professores(string email)
        {
            Session["Lista"] = "tem";
            return View(CadastroProfessores.ListaProfessor(email));
        }

        public ActionResult CadastroProfessor()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastroProfessor(string email, string nome,  int nif, string telefone, string areaAtuacao, string cep, string cidade, string endereco, string pais, string uf, string bairro)
        {
            Session["Lista"] = null;
            CadastroProfessores c = new CadastroProfessores();
            c.Email = email;
            c.Nome = nome;
            c.CodUsuario = nif;
            c.AreaAtuacao = areaAtuacao;
            c.Cep = cep;
            c.Cidade = cidade;
            c.Endereco = endereco;
            c.Pais = pais;
            c.Uf = uf;
            c.Bairro = bairro;
            c.Telefone = telefone;

            TempData["Msg"] = c.Cadastrar();

            return RedirectToAction("CadastroProfessor");
        }

        public ActionResult EditarProfessor(string email)
        {
            Session["Lista"] = null;
            CadastroProfessores c = CadastroProfessores.BuscaProfessor(email);

            if (c == null)
            {
                TempData["Msg"] = "Professor não encontrado no sistema!";
                return RedirectToAction("Professores");
            }

            return View(c);
        }

        [HttpPost]
        public ActionResult EditarProfessor(string email, string nome, string CodUsuario, string telefone, string areaAtuacao, string cep, string cidade, string endereco, string pais, string uf, string bairro)
        {
            Session["Lista"] = null;
            CadastroProfessores c = new CadastroProfessores();
            c.Email = email;
            c.Nome = nome;
            c.CodUsuario = int.Parse(CodUsuario);
            c.AreaAtuacao = areaAtuacao;
            c.Cep = cep;
            c.Cidade = cidade;
            c.Endereco = endereco;
            c.Pais = pais;
            c.Uf = uf;
            c.Bairro = bairro;
            c.Telefone = telefone;

            string res = c.Editar();
            TempData["Msg"] = res;
            if (res == "Editado com sucesso!")
            {
                return RedirectToAction("Professores");
            }
            else
            {
                return RedirectToAction("EditarProfessor");
            }
        }

        public ActionResult ExcluirProfessor(string email)
        {
            CadastroProfessores c = new CadastroProfessores();
            c.Email = email;
            TempData["Msg"] = c.Remover();
            return RedirectToAction("Professores");
        }

        /*------------------------------------------------------Professor--------------------------------------------------------------*/



        /*------------------------------------------------------Livros--------------------------------------------------------------*/
        public ActionResult Livros()
        {
            Session["Lista"] = null;
            return View();
        }

        public ActionResult LerMais(int NumTombo)
        {
            Session["Lista"] = null;
            return View(LivrosModel.LerMais(NumTombo));
        }

        [HttpPost]
        public ActionResult Livros(string titulo)
        {
            Session["Lista"] = titulo;
            
            return View(LivrosModel.Lista(titulo));
        }

       

        public ActionResult CadastroLivros()
        {
            Session["Lista"] = null;
            return View(AutoresModel.ListaTodos());
        }

        [HttpPost]
        public ActionResult CadastroLivros(string isbn, int serie, string titulo, string subtitulo, string descritores, string autor)
        {
            Session["Lista"] = null;
            LivrosModel l = new LivrosModel();
            l.Isbn = isbn;
            l.Serie = serie;
            l.Titulo = titulo;
            l.Descritores = descritores;
            l.CodAutor = int.Parse(autor.ToString());

            Session["ISBN"] = isbn;

            if (subtitulo == null)
            {
                l.Subtitulo = " ";
            }
            else
            {
                l.Subtitulo = subtitulo;
            }

            TempData["Msg"] = l.CadastrarLivro();

            return RedirectToAction("CadastroCopia");
        }
        public ActionResult CadastroCopia()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastroCopia(string isbn, int numT, int edicao, string cidade, int anoP, string editora, int numP)
        {
            Session["Lista"] = null;
            LivrosModel l = new LivrosModel();
            l.Isbn = isbn;
            l.NumTombo = numT;
            l.Edicao = edicao;
            l.CidadePubli = cidade;
            l.AnoPubli = anoP;
            l.Editora = editora;
            l.NumPag = numP;
            

            foreach (string nomeArq in Request.Files)
            {
                HttpPostedFileBase arquivoPostado = Request.Files[nomeArq];
                int tamanhoImg = arquivoPostado.ContentLength; //pega o tamanho do arquivo
                string tipoArq = arquivoPostado.ContentType; //pega o tipo do arquivo

                //se existe a palavra jpeg dentro do arquivo
                if (tipoArq.IndexOf("jpeg") > 0)
                {
                    byte[] img = new byte[tamanhoImg];
                    arquivoPostado.InputStream.Read(img, 0, tamanhoImg); //Ler do primeiro byte (0) até o último (tamanho da imagem) e guardar na variável imgBytes

                    l.Img = img;
                    
                }
            }


            TempData["Msg"] = l.CadastrarCopia();

            return RedirectToAction("CadastroCopia");
        }

        
        public ActionResult EditarCopia(int numTombo)
        {
            Session["Lista"] = null;
            LivrosModel l = LivrosModel.BuscaLivro(numTombo);

            Session["NumTombo"] = numTombo;

            if (l == null)
            {
                TempData["Msg"] = "Cópia não encontrada no sistema!";
                return RedirectToAction("Livros");
            }
            

            return View(l);
        }

        [HttpPost]
        public ActionResult EditarCopia(string Isbn, int NumTombo, int Edicao, string CidadePubli, int AnoPubli, string Editora, int NumPag)
        {
            Session["Lista"] = null;
            LivrosModel l = new LivrosModel();
            l.NumTombo = int.Parse(Session["NumTombo"].ToString());
            l.Edicao = Edicao;
            l.CidadePubli = CidadePubli;
            l.AnoPubli = AnoPubli;
            l.Editora = Editora;
            l.NumPag = NumPag;
            l.Isbn = Isbn;

            foreach (string nomeArq in Request.Files)
            {
                HttpPostedFileBase arquivoPostado = Request.Files[nomeArq];
                int tamanhoImg = arquivoPostado.ContentLength; //pega o tamanho do arquivo
                string tipoArq = arquivoPostado.ContentType; //pega o tipo do arquivo

                //se existe a palavra jpeg dentro do arquivo
                if (tipoArq.IndexOf("jpeg") > 0)
                {
                    byte[] img = new byte[tamanhoImg];
                    arquivoPostado.InputStream.Read(img, 0, tamanhoImg); //Ler do primeiro byte (0) até o último (tamanho da imagem) e guardar na variável imgBytes

                    l.Img = img;

                }
            }

            string res = l.EditarCopia(NumTombo);
            TempData["Msg"] = res;
            if (res == "Cópia editada com sucesso!")
            {
                return RedirectToAction("Livros");
            }
            else
            {
                return RedirectToAction("EditarCopia");
            }
        }

        public ActionResult EditarLivro(int numTombo)
        {
            Session["Lista"] = null;
            LivrosModel l = LivrosModel.BuscaLivro(numTombo);

            if (l == null)
            {
                TempData["Msg"] = "Livro não encontrado no sistema!";
                return RedirectToAction("Livros");
            }


            return View(l);
        }

        [HttpPost]
        public ActionResult EditarLivro(string Isbn, int Serie, string Titulo, string Subtitulo, string Descritores)
        {
            Session["Lista"] = null;
            LivrosModel l = new LivrosModel();
            l.Serie = Serie;
            l.Titulo = Titulo;
            l.Subtitulo = Subtitulo;
            l.Descritores = Descritores;
            l.Isbn = l.BuscaIsbn(Serie);
            
            string res = l.EditarLivro(Isbn);
            TempData["Msg"] = res;
            if (res == "Livro editado com sucesso!")
            {
                return RedirectToAction("Livros");
            }
            else
            {
                return RedirectToAction("EditarLivro");
            }
        }
        
        public ActionResult ExcluirLivro(string isbn)
        {
            LivrosModel l = new LivrosModel();
            l.Isbn = isbn;
            TempData["Msg"] = l.Remover();
            return RedirectToAction("Livros");
        }

        public ActionResult ExcluirCopia(int numTombo)
        {
            LivrosModel l = new LivrosModel();
            l.NumTombo = numTombo;
            TempData["Msg"] = l.RemoverCopia();
            return RedirectToAction("Livros");
        }
        /*------------------------------------------------------Livros--------------------------------------------------------------*/


        /*------------------------------------------------------Autores--------------------------------------------------------------*/
        public ActionResult Autores()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult Autores(string nome)
        {
            Session["Lista"] = "tem";
            return View(AutoresModel.Lista(nome));
        }


        public ActionResult CadastroAutor()
        {
            Session["Lista"] = null;
            return View();
        }

        [HttpPost]
        public ActionResult CadastroAutor(string nome)
        {
            Session["Lista"] = null;
            AutoresModel a = new AutoresModel();
            a.NomeAutor = nome;

            TempData["Msg"] = a.CadastrarAutor();

            return RedirectToAction("CadastroAutor");
        }

        [ActionName("EditarAutor")]
            public ActionResult EditarAutores(string nome)
        {
            Session["Lista"] = null;
            AutoresModel a = AutoresModel.BuscaAutor(nome);
            Session["NomeAutor"] = nome;

            if (a == null)
            {
                TempData["Msg"] = "Livro não encontrado no sistema!";
                return RedirectToAction("Autores");
            }

            return View("EditarAutores", a);
        }


        [HttpPost]
        public ActionResult EditarAutor(string nome)
        {
            Session["Lista"] = null;
            AutoresModel a = new AutoresModel();
            a.NomeAutorEdit = nome;
            a.NomeAutor = Session["NomeAutor"].ToString();

            string res = a.Editar();
            TempData["Msg"] = res;
            if (res == "Editado com sucesso!")
            {
                return RedirectToAction("Autores");
            }
            else
            {
                return RedirectToAction("EditarAutor");
            }
        }


        public ActionResult ExcluirAutor(int id)
        {
            AutoresModel a = new AutoresModel();
            a.Cod = id;
            TempData["Msg"] = a.Remover();
            return RedirectToAction("Autores");
        }
        /*------------------------------------------------------Autores--------------------------------------------------------------*/
    }
    }
