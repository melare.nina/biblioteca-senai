﻿using ProjetoBiblioteca.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ProjetoBiblioteca
{
    /// <summary>
    /// Summary description for Usuario
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Usuario : System.Web.Services.WebService
    {

        [WebMethod]
        public  List<LivrosModel> Lista(string titulo)
        {
            List<LivrosModel> lista = new List<LivrosModel>();
            SqlConnection con = new SqlConnection("Server=ESN509VMSSQL; Database=MARINA MELARÉ; User id=Aluno; Password=Senai1234");
            try
            {

                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Livros l, Copiass c, Livros_Autoor la, Autoor a WHERE l.isbn= c.isbn and l.isbn = la.ISBN and a.CodAutor = la.CodAutor ", con);



                if (titulo != "")
                {
                    query.CommandText = query.CommandText + "AND l.Titulo = @Titulo";
                    query.Parameters.AddWithValue("@Titulo", titulo);

                }


                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    LivrosModel l = new LivrosModel();
                    l.Titulo = leitor["Titulo"].ToString();
                    l.Isbn = leitor["ISBN"].ToString();
                    l.Serie = int.Parse(leitor["Serie"].ToString());
                    l.Subtitulo = leitor["Subtitulo"].ToString();
                    l.Descritores = leitor["Descritores"].ToString();
                    l.QtdAva = int.Parse(leitor["QtdAvaliadores"].ToString());
                    l.Avaliacao = int.Parse(leitor["Avaliacao"].ToString());
                    l.NumTombo = int.Parse(leitor["NumeroDeTombo"].ToString());
                    l.Edicao = int.Parse(leitor["Edicao"].ToString());
                    l.CidadePubli = leitor["CidadePubli"].ToString();
                    l.AnoPubli = int.Parse(leitor["AnoPubli"].ToString());
                    l.Editora = leitor["Editora"].ToString();
                    l.NumPag = int.Parse(leitor["NumPaginas"].ToString());
                    l.Img = (byte[])leitor["imagem"];
                    l.Situacao = leitor["situacao"].ToString();
                    l.NomeAutor = leitor["Nome"].ToString();

                    //Converte a imagem para string
                    l.ImagemString = Convert.ToBase64String(l.Img);

                    lista.Add(l);
                }

            }
            catch (Exception e)
            {
                lista = new List<LivrosModel>();
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return lista;
        }




        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }



        [WebMethod]
        public string Renovar(int numerodeTombo, string dataRenovaa, int renovacao)
        {
            SqlConnection con = new SqlConnection("Server=ESN509VMSSQL; Database=MARINA MELARÉ; User id=Aluno; Password=Senai1234");
            DateTime dataRenova = new DateTime();
            try
            {
                con.Open();

                SqlCommand query = new SqlCommand("SELECT * FROM Emprestimos Where NumeroDeTombo = @ntombo", con);
                query.Parameters.AddWithValue("@ntombo", numerodeTombo);
                SqlDataReader leitor = query.ExecuteReader();


                while (leitor.Read())
                {
                    dataRenova = DateTime.Parse(dataRenovaa);
                    dataRenova = (DateTime)leitor["DataDevolu"];
                    renovacao = int.Parse(leitor["Renovacao"].ToString());
                }



                leitor.Close();

                if (renovacao <= 3)
                {
                    dataRenova = dataRenova.AddDays(15);


                    query = new SqlCommand("UPDATE Emprestimos SET DataDevolu = @dataD, Renovacao = @renovacao WHERE NumeroDeTombo = @ndetombo", con);
                    query.Parameters.AddWithValue("@dataD", dataRenova);
                    query.Parameters.AddWithValue("@renovacao", renovacao + 1);
                    query.Parameters.AddWithValue("@ndetombo", numerodeTombo);
                    query.ExecuteNonQuery();



                }
                else
                {
                    return "Não foi possível realizar a renovação";
                }

                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }



            }

            catch (Exception ee)
            {
                return (ee.Message);
            }

            return "Renovação efetuada com sucesso";

        }


        [WebMethod]
       public bool validaLogin(String email, String senha)
        {
            bool loginValido = false;
            SqlConnection con = new SqlConnection("Server=ESN509VMSSQL; Database=MARINA MELARÉ; User id=Aluno; Password=Senai1234");

            con.Open();

            SqlCommand query = new SqlCommand("SELECT * FROM Usuario WHERE Email = @email AND Senha = @senha", con);
            query.Parameters.AddWithValue("@email", email);
            query.Parameters.AddWithValue("@senha", senha);
            //Pegando a tabela, mandando executar e jogando ela na minha variável do tipo DataReader
            SqlDataReader leitor = query.ExecuteReader();

            loginValido = leitor.HasRows;

            con.Close();

            return loginValido;
        }

        [WebMethod]
        public  CadastroAlunos BuscaAluno(string email)
        {
            CadastroAlunos c = new CadastroAlunos();
            SqlConnection con = new SqlConnection("Server=ESN509VMSSQL; Database=MARINA MELARÉ; User id=Aluno; Password=Senai1234");
            try
            {
               
                con.Open();
                SqlCommand query =
                    new SqlCommand("SELECT * FROM Usuario WHERE Email = @Email", con);
                query.Parameters.AddWithValue("@email", email);
                SqlDataReader leitor = query.ExecuteReader();

                while (leitor.Read())
                {
                    c.Email = leitor["Email"].ToString();
                    c.Nome = leitor["Nome"].ToString();
                    c.Senha = leitor["Senha"].ToString();
                    c.AreaAtuacao = leitor["AreaAtuacao"].ToString();
                    c.Cep = leitor["CEP"].ToString();
                    c.Cidade = leitor["Cidade"].ToString();
                    c.Endereco = leitor["Endereço"].ToString();
                    c.Pais = leitor["País"].ToString();
                    c.Uf = leitor["UF"].ToString();
                    c.Bairro = leitor["Bairro"].ToString();
                }
            }
            catch (Exception e)
            {
                c = null;
            }

            if (con.State == ConnectionState.Open)
                con.Close();

            return c;
        }


     

    }
}
