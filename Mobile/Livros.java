package lmcompany.projetobibliotecamobile;

import android.app.Dialog;
import android.app.ListFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Livros.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Livros#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Livros extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ImageButton btnVoz;
    private EditText edtxtPesquisa;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Dialog popup;
    private TextView txt1,txt2;
    private ImageView imgCapinha;
    private ImageButton btnfecha;
    private View v;
    ArrayList<ModelodeListaE> lista = new ArrayList<>();
    private ListView l;
    private OnFragmentInteractionListener mListener;

    public Livros() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Livros.
     */
    // TODO: Rename and change types and number of parameters
    public static Livros newInstance(String param1, String param2) {
        Livros fragment = new Livros();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



        v = inflater.inflate(R.layout.fragment_livros, container, false);

        btnVoz = (ImageButton) v.findViewById(R.id.btnVoz);
        edtxtPesquisa = (EditText) v.findViewById(R.id.edtxtPesquisa);


        btnVoz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                try{
                    startActivityForResult(i,200);


                }catch (ActivityNotFoundException a){

                }

            }
        });

        popup = new Dialog(this.getContext());
     l= (ListView) v.findViewById(R.id.lv);

     adptr a = new adptr(this.getActivity(),getModelodeListae());

l.setAdapter(a);


l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

//        Intent intent = new Intent(getContext(), MainActivity.class);
//
//        Bundle dados = new Bundle();
//
///*                dados.putString("nome", nome);
//                dados.putString("email", email);*/
//        dados.putString("titulo",lista.get(i).titulo);
//        dados.putString("autor",lista.get(i).autor);
//        dados.putInt("img",lista.get(i).img);
//        intent.putExtras(dados);
//
//        startActivity(intent);
//
//
//
//



        popup.setContentView(R.layout.popuplivros);

        txt1 = (TextView) popup.findViewById(R.id.txt11);
        txt2 = (TextView) popup.findViewById(R.id.txt2);
        imgCapinha = (ImageView) popup.findViewById(R.id.imgCapinha);
        btnfecha = (ImageButton) popup.findViewById(R.id.btnfecha);



btnfecha.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        popup.dismiss();
    }
});










        txt1.setText(txt1.getText()+" "+lista.get(i).titulo);
      txt2.setText(txt2.getText()+" "+lista.get(i).autor);
      // imgCapinha.setImageResource(lista.get(i).img);

        popup.show();


    }
});


        return v;
    }



    public void ShowPopup(View view){


    }





    private ArrayList<ModelodeListaE> getModelodeListae(){

        try {
            //IP computador / nome_da_pasta / nome_web_service / nome_método
            String url = "http://10.87.106.3/ProjetoBiblioteca/Usuario.asmx/Lista";
            String retorno = new ConectaHTTP(url, new String[]{}, new String[]{}).execute("").get();

            //Método para ler um XML com categorias (quando o webservice retorna uma lista)
            BuscaConteudoXML xml = new BuscaConteudoXML(retorno, "LivrosModel");
            NodeList elementos = xml.elementosXML();

            //Para cada item encontrado à partir do ponto principal
            for (int i = 0; i < elementos.getLength(); i++) {
                Node node = elementos.item(i);

                //Testando se existe um item dentro da estrutura "Produto"
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    //Testes


//                    Categorias c = new Categorias(Integer.parseInt(xml.getValue("id", element)), xml.getValue("nome", element).toString());
//                    lista.add(c); //Adicionando os itens na lista
                    String b = xml.getValue("Img", element);
                    byte[] bt = Base64.decode(b,0);

                    Bitmap bitmap = BitmapFactory.decodeByteArray(bt, 0, bt.length);

                    ModelodeListaE l;
                    l = new ModelodeListaE(xml.getValue("Titulo", element).toString(),xml.getValue("Autor", element).toString(),bitmap);
                    lista.add(l);



                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

//        ModelodeListaE l;
//
//        l = new ModelodeListaE("Inferno","Dan Brown", R.drawable.inferno);
//        lista.add(l);
//
//        l = new ModelodeListaE("Um dia","David Nicholls", R.drawable.umdia);
//        lista.add(l);
//
//        l = new ModelodeListaE("Como eu era antes de Você","Jojo Moyes", R.drawable.ceav);
//        lista.add(l);
//
//        l = new ModelodeListaE("Inferno","Dan Brown", R.drawable.inferno);
//        lista.add(l);
//
//        l = new ModelodeListaE("Um dia","David Nicholls", R.drawable.umdia);
//        lista.add(l);
//
//        l = new ModelodeListaE("Como eu era antes de Você","Jojo Moyes", R.drawable.ceav);
//        lista.add(l);
//
//        l = new ModelodeListaE("Inferno","Dan Brown", R.drawable.inferno);
//        lista.add(l);
//
//        l = new ModelodeListaE("Um dia","David Nicholls", R.drawable.umdia);
//        lista.add(l);
//
//        l = new ModelodeListaE("Como eu era antes de Você","Jojo Moyes", R.drawable.ceav);
//        lista.add(l);

        return lista;

    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(requestCode == 200){
//            if(resultCode == RESULT_OK && data!= null ){
//                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//                edtxtPesquisa.setText(result.get(0));
//            }
//        }
//
//    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
