package lmcompany.projetobibliotecamobile;

import android.graphics.Bitmap;

/**
 * Created by aluno on 09/05/2018.
 */

public class ModelodeListaE
{

    String titulo;
    String autor;
    Bitmap img;


    public  ModelodeListaE(String titulo, String autor,Bitmap img){

        this.titulo = titulo;
        this.autor = autor;
        this.img = img;
    }

    public String getTitulo(){
        return titulo;
}

    public String getAutor(){
        return autor;
    }

    public Bitmap getImg(){
        return img;
    }

}
