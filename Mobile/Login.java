package lmcompany.projetobibliotecamobile;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabItem;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;


public class Login extends Activity {
   int a=0;

    private Button btn;
    private ImageView Logo;
    private EditText txtEmail, txtSenha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Logo = (ImageView) findViewById(R.id.Logo);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);







        final CircularProgressButton btn = (CircularProgressButton) findViewById(R.id.btn_id);





        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                @SuppressLint("StaticFieldLeak") AsyncTask<String,String,String> demoLogin = new AsyncTask<String, String, String>() {
                    @Override
                    protected String doInBackground(String... params) {
                        try{
                            Thread.sleep(1000);

                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                        return "done";
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        if(s.equals("done")){




String [] parametros = {"email","senha"};
String [] valores= {txtEmail.getText().toString(),txtSenha.getText().toString()};


try{
    String retorno = new ConectaHTTP("http://10.87.106.3/ProjetoBiblioteca/Usuario.asmx/validaLogin",parametros,valores).execute("").get();

Log.i("logLogin", retorno);
    if(a==0){
        btn.doneLoadingAnimation(Color.GREEN,BitmapFactory.decodeResource(getResources(),R.drawable.ic_done_white_48dp) );


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                Animation deslocaLogo = new TranslateAnimation(0,0,-350,0);

                deslocaLogo.setFillAfter(true);
                deslocaLogo.setDuration(1000);
                Logo.startAnimation(deslocaLogo);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        btn.setVisibility(View.INVISIBLE);
                        txtEmail.setVisibility(View.INVISIBLE);
                        txtSenha.setVisibility(View.INVISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                Intent i = new Intent(Login.this, Principal.class);
                                startActivity(i);

finish();

                            }
                        }, 1000);



                    }
                }, 100);

            }
        }, 1000);





    }else{
        btn.doneLoadingAnimation(Color.RED,BitmapFactory.decodeResource(getResources(),R.drawable.x) );

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                btn.revertAnimation();

            }
        }, 900);

        btn.setBackground(getDrawable(R.drawable.shape_default));


    }




}catch (Exception e){

   e.printStackTrace();

    Log.i("logLogin", e.getMessage());
}


                        }


                    }
                };

                btn.startAnimation();
                demoLogin.execute();

            }
        });



        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Animation deslocaLogo = new TranslateAnimation(0,0,0,-350);
                deslocaLogo.setFillAfter(true);
                deslocaLogo.setDuration(1000);
                Logo.startAnimation(deslocaLogo);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        btn.setVisibility(View.VISIBLE);
                        txtEmail.setVisibility(View.VISIBLE);
                        txtSenha.setVisibility(View.VISIBLE);

                    }
                }, 1000);

            }
        }, 1000);







    }
}

