package lmcompany.projetobibliotecamobile;

/**
 * Created by aluno on 09/05/2018.
 */

public class ModeloEmp {


    String titulo;
    String dataE;
    String dataD;
    int img;


    public  ModeloEmp(String titulo, String dataE,String dataD,int img){

        this.titulo = titulo;
        this.dataE = dataE;
        this.dataD = dataD;
        this.img = img;
    }

    public String getTitulo(){
        return titulo;
    }

    public String getDataE(){
        return dataE;
    }
    public String getDataD(){
        return dataD;
    }

    public int getImg(){
        return img;
    }

}


