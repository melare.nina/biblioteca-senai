package lmcompany.projetobibliotecamobile;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.Locale;

import static lmcompany.projetobibliotecamobile.R.id.na_Emp;
import static lmcompany.projetobibliotecamobile.R.id.nav_Hiist;
import static lmcompany.projetobibliotecamobile.R.id.nav_Livro;
import static lmcompany.projetobibliotecamobile.R.id.tabEmp;
import static lmcompany.projetobibliotecamobile.R.id.tabLivro;


public class Principal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,
        emprestimo.OnFragmentInteractionListener, Livros.OnFragmentInteractionListener,
        Historico.OnFragmentInteractionListener{

    private TabItem tabEmp, tabLivro, TabHist;
    private  TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);






        TabLayout tabLayout =  (TabLayout) findViewById(R.id.tabs);
        ViewPager ola = (ViewPager) findViewById(R.id.ola);

        tabpagerAdapter t = new tabpagerAdapter(getSupportFragmentManager());


        ola.setAdapter(t);

        tabLayout.setupWithViewPager(ola);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if(tab.getPosition()== 0){
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    navigationView.setCheckedItem(na_Emp);

                }

                if(tab.getPosition() == 1){
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    navigationView.setCheckedItem(nav_Livro);

                }

                if(tab.getPosition() == 2){
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    navigationView.setCheckedItem(nav_Hiist);

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.na_Emp);




        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        emprestimo  e = new emprestimo();
        transaction.replace(R.id.ola, e);
        transaction.commit();

    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.principal, menu);
        return true;
    }







    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();





        if (id == R.id.na_Emp) {

            ViewPager ola = (ViewPager) findViewById(R.id.ola);
            ola.setCurrentItem(0);
        } else if (id == R.id.nav_Livro) {

            ViewPager ola = (ViewPager) findViewById(R.id.ola);
            ola.setCurrentItem(1);
        } else if (id == R.id.nav_Hiist) {

            ViewPager ola = (ViewPager) findViewById(R.id.ola);
            ola.setCurrentItem(2);
        } else if (id == R.id.nav_Sair) {
            Intent i = new Intent(Principal.this, Login.class);
            startActivity(i);
        }else if( id == R.id.nav_config){
            Intent o = new Intent(Principal.this, Configuracao.class);
            startActivity(o);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        //transaction.commit();
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
